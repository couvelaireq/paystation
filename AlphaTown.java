package implementation;

public class AlphaTown implements TownFactory {
	private LinearRateStrategy lrs;
	private StandardReceipt sr;
	private endTimeDisplayStrategy etd;
	
	public AlphaTown(LinearRateStrategy lrs, StandardReceipt sr, endTimeDisplayStrategy etd) {
		this.lrs = lrs;
		this.sr = sr;
		this.etd = etd;
	}

	@Override
	public RateStrategy getRateStrategy() {
		return lrs;
	}

	@Override
	public Receipt getReceipt() {
		return sr;
	}

	@Override
	public DisplayStrategy getDisplayStrategy() {
		return etd;
	}

}
