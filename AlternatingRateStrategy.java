package implementation;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AlternatingRateStrategy implements RateStrategy{
	private LinearRateStrategy linearRateStrategy;
	private ProgressiveRateStrategy progressiveRateStrategy;
	
	public AlternatingRateStrategy() {
		linearRateStrategy = new LinearRateStrategy();
		progressiveRateStrategy = new ProgressiveRateStrategy();
	}
	@Override
	public int calculateTime(int amount) {
		if (isWeekend()) { // if weekend use progressive strategy for rate
			return progressiveRateStrategy.calculateTime(amount);
		}
		return linearRateStrategy.calculateTime(amount);
	}
	
	private boolean isWeekend() {
		Date d = new Date();
		Calendar c = new GregorianCalendar();
		c.setTime(d);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		if(dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
			return true;
		}
		return false;
	}
	

}
