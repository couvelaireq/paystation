package implementation;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class BarcodeReceipt implements Receipt {
	private int value;
	
	public BarcodeReceipt(int value) {
		this.value = value;
	}

	@Override
	public int value() {
		return value;
	}
	
	public void printReceipt() {
		System.out.println("Parking Receipt" + "\n");
		System.out.println("Value:" + value);
		Date d = new Date();
		Calendar c = new GregorianCalendar();
		c.setTime(d);
		int hour = c.get(Calendar.HOUR);
		int min = c.get(Calendar.MINUTE);
		System.out.println("Car Parked at " + hour + ":" + min);
		System.out.println("|| |||| |||||||| || || | ||||||||");
	}

	@Override
	public String getTypeString() {
		return "barcode";
	}

}
