package implementation;

public class BetaTown implements TownFactory {
	private ProgressiveRateStrategy prs;
	private BarcodeReceipt br;
	private minutesDisplayStrategy md;
	
	public BetaTown(ProgressiveRateStrategy prs, BarcodeReceipt br, minutesDisplayStrategy md) {
		this.prs = prs;
		this.br = br;
		this.md = md;
	}

	@Override
	public RateStrategy getRateStrategy() {
		return prs;
	}

	@Override
	public Receipt getReceipt() {
		return br;
	}

	@Override
	public DisplayStrategy getDisplayStrategy() {
		return md;
	}

}
