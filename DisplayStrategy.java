package implementation;

public interface DisplayStrategy {

	public int readDisplay(int timeBought);
}
