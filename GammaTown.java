package implementation;

public class GammaTown implements TownFactory {
	private AlternatingRateStrategy ars;
	private StandardReceipt sr;
	private minutesDisplayStrategy mds;
	
	public GammaTown(AlternatingRateStrategy ars, StandardReceipt sr, minutesDisplayStrategy mds) {
		this.ars = ars;
		this.sr = sr;
		this.mds = mds;
	}

	@Override
	public RateStrategy getRateStrategy() {
		return ars;
	}

	@Override
	public Receipt getReceipt() {
		return sr;
	}

	@Override
	public DisplayStrategy getDisplayStrategy() {
		return mds;
	}

}
