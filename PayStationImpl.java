package implementation;

/**
 * Implementation of the pay station.
 * 
 * Responsibilities:
 * 
 * 1) Accept payment; 2) Calculate parking time based on payment; 3) Know
 * earning, parking time bought; 4) Issue receipts; 5) Handle buy and cancel
 * events.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Computer Science Department Aarhus University
 * 
 * This source code is provided WITHOUT ANY WARRANTY either expressed or
 * implied. You may study, use, modify, and distribute it for non-commercial
 * purposes. For any commercial use, see http://www.baerbak.com/
 */

public class PayStationImpl implements PayStation {
	private int insertedSoFar;
	private int timeBought;
	private TownFactory town;

	public PayStationImpl(TownFactory town) {
		this.town = town;
	}

	public void changeTown(TownFactory town) {
		this.town = town;
	}

	public void addPayment(int coinValue) throws IllegalCoinException {
		switch (coinValue) {
		case 5:
			break;
		case 10:
			break;
		case 25:
			break;
		default:
			throw new IllegalCoinException("Invalid coin: " + coinValue);
		}
		insertedSoFar += coinValue;
		timeBought = town.getRateStrategy().calculateTime(insertedSoFar);
	}

	public int readDisplay() {
		return town.getDisplayStrategy().readDisplay(timeBought);
	}

	public Receipt buy() {
		Receipt r;
		if (town.getReceipt().getTypeString() == "barcode") {
			r = new BarcodeReceipt(timeBought);
		}
		r = new StandardReceipt(timeBought);
		reset();
		return r;
	}

	public void cancel() {
		reset();
	}

	private void reset() {
		timeBought = insertedSoFar = 0;
	}
}
