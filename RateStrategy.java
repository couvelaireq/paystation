package implementation;

public interface RateStrategy {
	
	// calculate the time of parking bought
	public int calculateTime(int amount);
}
