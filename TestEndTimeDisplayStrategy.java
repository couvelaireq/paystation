package implementation;

import org.junit.Before;
import org.junit.Test;

public class TestEndTimeDisplayStrategy {
	endTimeDisplayStrategy etd;
	
	@Before
	public void setUp() {
		etd = new endTimeDisplayStrategy();
	}
	
	@Test
	public void test() {
		System.out.println(etd.readDisplay(5));
		System.out.println(etd.readDisplay(20));
		System.out.println(etd.readDisplay(60));
	}
}
