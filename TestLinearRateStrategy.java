package implementation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestLinearRateStrategy {
	LinearRateStrategy ls;
	
	@Before
	public void setUp() {
		ls = new LinearRateStrategy();
	}
	
	@Test
	public void FiveCentsGivesTwoMinsParking() {
		assertEquals(2, ls.calculateTime(5));
	}
	
	@Test
	public void TenCentsGivesFourMinsParking() {
		assertEquals(4, ls.calculateTime(10));
	}
	
	@Test 
	public void FiftyCentsGivesTwentyMinsParking() {
		assertEquals(20, ls.calculateTime(50));
	}
}
