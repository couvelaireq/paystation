package implementation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestProgressiveRateStrategy {
	ProgressiveRateStrategy ps;
	
	@Before
	public void setUp() {
		ps = new ProgressiveRateStrategy();
	}
	
	@Test
	public void shouldDisplay60MinFor150Cent() {
		assertEquals(60, ps.calculateTime(150));
	}
	
	@Test
	public void shouldDisplay120MinFor350Cent() {
		assertEquals(120, ps.calculateTime(350));
	}
	
	@Test
	public void shouldDisplay180MinFor650Cent() {
		assertEquals(180, ps.calculateTime(650));
	}
}
