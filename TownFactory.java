package implementation;

public interface TownFactory {
	public RateStrategy getRateStrategy();
	public Receipt getReceipt();
	public DisplayStrategy getDisplayStrategy();
	
}
