package implementation;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class endTimeDisplayStrategy implements DisplayStrategy {

	// returns the final ending parking time (current time + timeBought)
	// needs to handle roll over for minutes and hours
	@Override
	public int readDisplay(int timeBought) {
		Date d = new Date();
		Calendar c = new GregorianCalendar();
		c.setTime(d);
		int hour = c.get(Calendar.HOUR);
		int minute = c.get(Calendar.MINUTE);
		
		if(minute + timeBought < 60) {
			int newMinute = minute + timeBought;
			return concatenateInts(hour, newMinute);
		}
		else if(timeBought + minute >= 60) { // handle minute and hour roll over
			minute = minute + timeBought - 60;
			hour = hour + 1;
			if(hour > 24) {
				hour = hour - 24;
			}
		}
		return concatenateInts(hour, minute);
	}
	
	private int concatenateInts(int hour, int min) {
		String hourString = Integer.toString(hour);
		String minuteString = Integer.toString(min);
		
		String finalTime = hourString + minuteString;
		
		int c = Integer.parseInt(finalTime);
		
		return c;
	}

}
